from consts import BLOCK_SIZE, ROUNDS, S_BOX, INVERSE_S_BOX, STATE_MATRIX_SIZE
from key import schedule
from state import State
from utils import *
from galois import *
from pad import pad, unpad

class AES(object):
    def __init__(self, key):
        self.keys = schedule(key)
        self.state = None

    def get_blocks(self, m):
        blocks = pad(cut(m, BLOCK_SIZE))
        return map(str_to_int, blocks)

    def block(self):
        return self.state.block()

    def encrypt(self, pt):
        blocks = self.get_blocks(pt)
        ct = map(lambda b: self.encrypt_block(b), blocks)
        return ''.join(ct)
    
    def decrypt(self, ct):
        blocks = map(hex_block_to_int, cut(ct, 32))
        pt = unpad(map(lambda b: self.decrypt_block(b).decode('hex'), blocks))
        return ''.join(pt)
    
    def add_round_key(self, i):
        self.state = self.state ^ self.keys[i]

    def sub_bytes(self):
        self.state.apply(lambda x: S_BOX[x])

    def unsub_bytes(self):
        self.state.apply(lambda x: INVERSE_S_BOX[x])
    
    def shift_rows(self):
        self.state.apply_to_rows(shl)
    
    def unshift_rows(self):
        self.state.apply_to_rows(lambda x, i: x[-i:] + x[:-i])
        
    def mix(self, l):
        return [mul(shl(l, i)) for i in xrange(STATE_MATRIX_SIZE)]

    def unmix(self, l):
        return [unmul(shl(l, i)) for i in xrange(STATE_MATRIX_SIZE)]
    
    def mix_columns(self):
        columns = map(list, self.state.columns())
        mixed = map(lambda x: self.mix(x), columns)
        self.state = State(matrix_from_list(mixed))

    def unmix_columns(self):
        columns = map(list, self.state.columns())
        unmixed = map(lambda x: self.unmix(x), columns)
        self.state = State(matrix_from_list(unmixed))
    
    def init(self):
        self.add_round_key(0)

    def uninit(self):
        self.add_round_key(0)
    
    def rounds(self):
        for i in xrange(1, ROUNDS):
            self.sub_bytes()
            self.shift_rows()
            self.mix_columns()
            self.add_round_key(i)

    def unrounds(self):
        for i in xrange(ROUNDS - 1, 0, -1):
            self.add_round_key(i)
            self.unmix_columns()
            self.unshift_rows()
            self.unsub_bytes()
    
    def final(self):
        self.sub_bytes()
        self.shift_rows()
        self.add_round_key(ROUNDS)

    def unfinal(self):
        self.add_round_key(ROUNDS)
        self.unshift_rows()
        self.unsub_bytes()
    
    def encrypt_block(self, block):
        self.state = State(block)
        self.init()
        self.rounds()
        self.final()
        return self.block()
                
    def decrypt_block(self, block):
        self.state = State(block)
        self.unfinal()
        self.unrounds()
        self.uninit()
        return self.block()

if __name__ == "__main__":
    key = 'b48aca0e52c444d591fb306aaadc6377'
    a = AES(key)
    message = 'encrypt me please'
    ct = a.encrypt(message)
    pt = a.decrypt(ct)
    print message
    print ct 
    print pt

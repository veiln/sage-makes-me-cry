from consts import BLOCK_SIZE

def pad(blocks):
    l = BLOCK_SIZE - len(blocks[-1])
    blocks[-1] += chr(l) * l
    return blocks

def unpad(blocks):
    block = blocks[-1]
    val = ord(block[-1])
    
    if val > BLOCK_SIZE - 1 or val == 0:
        return blocks

    blocks[-1] = block[:-val]
    return blocks

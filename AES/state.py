from utils import *
from sage.all import matrix as sage_matrix
from numbers import Complex

class State(object):
    def __init__(self, block):
        if isinstance(block, Complex):
            block = words(block)

        if isinstance(block, type(sage_matrix())):
            self.matrix = block
            return
        
        m = matrix(map(lambda b: bytes_(b), block))
        self.matrix = m.transpose()
    
    def columns(self):
        return self.matrix.columns()
    
    def rows(self):
        return self.matrix.rows()
    
    def list(self):
        rows = map(list, self.rows())
        return reduce(lambda x, y: x + y, rows, [])

    def __xor__(self, m):
        r = map(lambda x: x[0] ^ x[1], zip(m.list(), self.list()))
        return State(matrix(r))

    def apply(self, f):
        self.matrix = self.matrix.apply_map(f)
    
    def apply_to_rows(self, f):
        rows = map(list, self.rows())
        rows = map(lambda (idx, val): f(val, idx), enumerate(rows))
        self.matrix = matrix(rows)
    
    def block(self):
        columns = self.columns()
        return ''.join(map(lambda c: ''.join(map(chr, c)), columns)).encode('hex')

    def show(self):
        print self.matrix, '\n'

def x1(val):
    return val

def x2(val):
    x2 = 2 * val
    if val >> 7:
        x2 ^= 0x1b
    return x2 & 0xff

def x3(val):
    return val ^ x2(val)

def x4(val):
    return x2(x2(val))

def x8(val):
    return x2(x4(val))

def x9(val):
    return x8(val) ^ val

def x11(val):
    return x9(val) ^ x2(val)

def x13(val):
    return x9(val) ^ x4(val)

def x14(val):
    return x8(val) ^ x4(val) ^ x2(val)

def mul(l):
    return x2(l[0]) ^ x3(l[1]) ^ x1(l[2]) ^ x1(l[3])

def unmul(l):
    return x14(l[0]) ^ x11(l[1]) ^ x13(l[2]) ^ x9(l[3])

from consts import HEX, HEX_BLOCK_SIZE, STATE_MATRIX_SIZE, S_BOX
import sage.all

def shl(l, i):
    return l[i:] + l[:i]

def str_to_int(b):
    return int('0x' + b.encode('hex').zfill(HEX_BLOCK_SIZE), HEX)

def hex_block_to_int(h):
    return hex_to_int(h, HEX_BLOCK_SIZE)

def hex_to_int(h, size=0):
    return int('0x' + h.zfill(size), HEX)

def int_to_hex(n, size=0):
    return hex(n)[2:].zfill(size)

def matrix(l):
    return sage.all.matrix(STATE_MATRIX_SIZE, l)

def matrix_from_columns(l):
    return sage.all.matrix(STATE_MATRIX_SIZE, l).transpose()

def matrix_from_list(l):
    return sage.all.matrix(STATE_MATRIX_SIZE, l).transpose()

def cut(chain, size):
    return [chain[i : size + i] for i in xrange(0, len(chain), size)]

def rearrange(words):
    while words[0] == 0:
        words = words[1:] + [0]
    return words

def words(m):
    h = hex(m)[2:-1]
    hex_words = cut(h.zfill(HEX_BLOCK_SIZE), 8)
    words = map(lambda x: hex_to_int(x), hex_words)
    return rearrange(words)

def bytes_(w):
    hex_bytes = cut(hex(w)[2:].zfill(8), 2)
    return map(lambda x: hex_to_int(x, 2), hex_bytes)

def join_bytes(l):
    return hex_to_int(''.join(map(lambda x: int_to_hex(x, 2), l)))

def rot(word):
    l = bytes_(word)
    return join_bytes(l[1:] + [l[0]])

def sub(word):
    return join_bytes(map(lambda x: S_BOX[x], bytes_(word)))


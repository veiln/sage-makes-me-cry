from consts import KEY_LEN, RCON, ROUNDS
from utils import rot, sub, words, hex_to_int, cut 
from state import State

def schedule(key):
    keys = words(hex_to_int(key))
    for i in xrange(4 * ROUNDS):
        if i % KEY_LEN == 0:
            keys.append(keys[-KEY_LEN] ^ rot(sub(keys[-1])) ^ (RCON[i // KEY_LEN] << 24))
        else:
            keys.append(keys[-1] ^ keys[-KEY_LEN])
    
    blocks = cut(keys, 4)
    return map(lambda b: State(b), blocks)

if __name__ == "__main__":
    map(lambda x: x.show(), schedule('61616161616161616161616161616161'))
